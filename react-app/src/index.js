import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import 'dotenv/config'

import { FirebaseAppProvider } from 'reactfire';

export const firebaseConfig = {
  apiKey: "AIzaSyDVPF6tzChMjm3NMnfF9MSatpnu8TTEGpc",
  authDomain: "cases-stripe-fireship.firebaseapp.com",
  projectId: "cases-stripe-fireship",
  storageBucket: "cases-stripe-fireship.appspot.com",
  messagingSenderId: "903423617716",
  appId: "1:903423617716:web:c39f0fd74595f3148405e1"
};

export const stripePromise = loadStripe('pk_test_51KKPYOIKOIVWzJ0ljsqbpw0rm3L3Z310NCScGtuzKyl0aTLKXw9eCXsBRrJEITvWj1cKBNvJLeODjJ3o4H5Shkm800kMB5aFiL');

ReactDOM.render(
  <React.StrictMode>
    <FirebaseAppProvider firebaseConfig={firebaseConfig}>
      <Elements stripe={stripePromise}>
        <App />
      </Elements>
    </FirebaseAppProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
