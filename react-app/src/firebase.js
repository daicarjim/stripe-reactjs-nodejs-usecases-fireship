import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

export const firebaseConfig = {
  apiKey: "AIzaSyDVPF6tzChMjm3NMnfF9MSatpnu8TTEGpc",
  authDomain: "cases-stripe-fireship.firebaseapp.com",
  projectId: "cases-stripe-fireship",
  storageBucket: "cases-stripe-fireship.appspot.com",
  messagingSenderId: "903423617716",
  appId: "1:903423617716:web:c39f0fd74595f3148405e1"
};

firebase.initializeApp(firebaseConfig)

export const db = firebase.firestore();
export const auth = firebase.auth();
